import { AngularFireAuth } from '@angular/fire/auth';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FuncionService {

  url: string = 'https://us-central1-mesero-app-backend.cloudfunctions.net/probar';

  constructor(private http: HttpClient, public afa: AngularFireAuth) { }

  probar() {
    this.afa.auth.currentUser.getIdToken().then(authToken => {
      const headers = new HttpHeaders({ 'Authorization': 'Bearer ' + authToken });

      const objUser = { uid: this.afa.auth.currentUser.uid };

      return this.http.post(this.url, objUser, { headers: headers }).toPromise().then(res => console.log(res));
    });
//    return this.http.get(this.url);
  }
}

import { LoginService } from './login.service';
import { Menu } from './../_model/menu';
import { AngularFireAuth } from '@angular/fire/auth';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { take, map, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';
import { MenuService } from './menu.service';


@Injectable({
  providedIn: 'root'
})
export class LoginGuardService implements CanActivate {
  private menus: Menu[] = [];

  constructor(private afa: AngularFireAuth, private router: Router, private menuService : MenuService, private loginService : LoginService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    
    
    return this.afa.authState
      .pipe(take(1))
      .pipe(map(authState => !!authState))
      .pipe(switchMap((autenticado: boolean) => {
        if (!autenticado) {
          this.router.navigate(['/login']);
          //return false;
          of(false);
        } else {

          let url = state.url;
          return this.menuService.listar().pipe(map((data) => {            
            this.menus = data;

          })).pipe(switchMap(() => {
            //let user_roles: string[] = ['USER', 'DBA', 'SYS', 'ADMIN'];                        

            return this.loginService.user.pipe(map(data => {
              if (data) {
                let user_roles: string[] = JSON.parse(JSON.stringify(data)).roles;
                //console.log(user_roles);
                let final_menus: Menu[] = [];

                for (let menu of this.menus) {
                  n2: for (let rol of menu.roles) {
                    for (let urol of user_roles) {
                      if (rol === urol) {
                        let m = new Menu();
                        m.nombre = menu.nombre;
                        m.icono = menu.icono;
                        m.url = menu.url;
                        final_menus.push(m);
                        break n2;
                      }
                    }
                  }
                }

                this.menuService.menu.next(final_menus);

                let cont = 0;
                for (let m of final_menus) {
                  if (m.url === url) {
                    cont++;
                    break;
                  }
                }

                if (cont > 0) {                  
                  return true;
                } else {
                  this.router.navigate(['not-403']);                  
                  return false;
                }
              }
            }));
          }));
        }
      }));


  }
}

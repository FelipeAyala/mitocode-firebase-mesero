import { Cliente } from './../_model/cliente';
import { AngularFirestore } from '@angular/fire/firestore';
import { Consumo } from './../_model/consumo';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConsumoService {

  constructor(private afs: AngularFirestore) {
  }
 
  registrar(consumo: Consumo) {
    return this.afs.collection('consumos').add({
      cliente: JSON.parse(JSON.stringify(consumo.cliente)),
      detalle: JSON.parse(JSON.stringify(consumo.detalle)),
      fechaPedido: consumo.fechaPedido,
      total: consumo.total
    });
  }

  registrarTransaccion(consumo: Consumo, cliente?: Cliente) {
    const batch = this.afs.firestore.batch();

    if (cliente != null || cliente != undefined) {
      const idGeneradoCliente = this.afs.createId();
      const docCliente = this.afs.collection('clientes').doc(idGeneradoCliente);

      batch.set(docCliente.ref, {
        id: idGeneradoCliente,
        dni: cliente.dni,
        nombreCompleto: cliente.nombreCompleto
      });

      consumo.cliente.id = idGeneradoCliente;
    }

    const idGeneradoConsumo = this.afs.createId();
    const docConsumo = this.afs.collection('consumos').doc(idGeneradoConsumo);

    batch.set(docConsumo.ref, {
      cliente: {
        id: consumo.cliente.id,
        nombreCompleto: consumo.cliente.nombreCompleto,
        dni: consumo.cliente.dni
      },
      fechaPedido: consumo.fechaPedido,
      total: consumo.total,
      detalle: JSON.parse(JSON.stringify(consumo.detalle))
    });

    return batch.commit();

  }
}

import { switchMap, first } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { Consumo } from '../_model/consumo';
import { forkJoin } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ReporteService {

  constructor(private afs: AngularFirestore) { }

  //consumos por fecha
  buscarPorFecha(fecha: Date) {
    let inicio = moment(fecha).toISOString();
    let fin = moment(inicio).add(1, 'days').toISOString();
    //console.log(inicio);
    //console.log(fin);

    return this.afs.collection('consumos', ref => ref.where('fechaPedido', '>=', new Date(inicio))
      .where('fechaPedido', '<', new Date(fin))).valueChanges();
  }

  //consumos por cliente
  buscarPorCliente() {
    return this.afs.collection<Consumo>('consumos', ref => ref.where('cliente.dni', '==', '72301306')).valueChanges();
  }

  //obtener datos del cliente por medio del consumo
  buscarClientePorConsumo() {
    return this.afs.collection('consumos', ref => ref.where('cliente.dni', '==', '72301306')).valueChanges().pipe(switchMap(data => {
      let idCliente = JSON.parse(JSON.stringify(data[0])).cliente.id;
      return this.afs.collection('clientes').doc(idCliente).valueChanges();
    }));
  }

  //forkJoin
  buscarConsumosClienteIden() {
    //let arreglo = [];
    let obs1 = this.afs.collection('consumos', ref => ref.where('cliente.dni', '==', '0000000')).valueChanges().pipe(first());
    let obs2 = this.afs.collection('consumos', ref => ref.where('cliente.dni', '>', '0000000')).valueChanges().pipe(first());

    //arreglo.push(obs1);
    //arreglo.push(obs2);

    //return forkJoin(arreglo);
    return forkJoin(obs1, obs2);

    //https://firebase.google.com/docs/firestore/query-data/queries?hl=es-419
  }
}

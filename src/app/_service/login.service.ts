import { Observable, EMPTY, Subject } from 'rxjs';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Injectable, Pipe, OnDestroy } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Usuario } from '../_model/usuario';
import { auth } from 'firebase/app';
import { switchMap, takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LoginService implements OnDestroy {

  private ngUnsubscribe: Subject<void> = new Subject();

  user: Observable<Usuario>;
  constructor(private afa: AngularFireAuth, private afs: AngularFirestore, private router: Router) {
    this.user = this.afa.authState.pipe(takeUntil(this.ngUnsubscribe)).pipe(
      switchMap(user => {
        if (user) {
          return this.afs.doc<Usuario>(`usuarios/${user.uid}`).valueChanges();
        } else {
          return EMPTY;
        }
      })
    );
  }

  login(usuario: string, clave: string) {
    return this.afa.auth.signInWithEmailAndPassword(usuario, clave).then(res => {
      return this.actualizarDataUsuario(res.user);
    });
  }

  loginGoogle() {
    const provider = new auth.GoogleAuthProvider();
    return this.oAuthLogin(provider);
  }

  loginFacebook() {
    const provider = new auth.FacebookAuthProvider();
    return this.oAuthLogin(provider);
  }

  registrarUsuario(usuario: string, clave: string) {
    return this.afa.auth.createUserWithEmailAndPassword(usuario, clave).then(res => {
      return this.actualizarDataUsuario(res.user);
    });
  }

  restablecerClave(email: string) {
    var auth = this.afa.auth;
    return auth.sendPasswordResetEmail(email);
  }

  actualizarDataUsuario(user: any) {
    const userRef: AngularFirestoreDocument<Usuario> = this.afs.doc(`usuarios/${user.uid}`);

    let sus = userRef.valueChanges().subscribe(data => {
      if (data) {
        const datos: Usuario = {
          uid: user.uid,
          email: user.email,
          roles: data.roles
        }
        return userRef.set(datos);
      } else {
        const datos: Usuario = {
          uid: user.uid,
          email: user.email,
          roles: ['USER']
        }
        return userRef.set(datos);
      }
    });

    sus.unsubscribe();
  }

  estaLogeado() {
    return this.afa.auth.currentUser != null;
  }

  cerrarSesion() {
    //this.afs.firestore.disableNetwork();
    return this.afa.auth.signOut().then((data) => {
      //window.location.reload();      
      this.router.navigate(['/login']);
    });
  }

  private oAuthLogin(provider: any) {
    return this.afa.auth.signInWithPopup(provider).then((credential) => {      
      this.actualizarDataUsuario(credential.user);
    });
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}

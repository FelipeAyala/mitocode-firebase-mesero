import { Cliente } from './../_model/cliente';
import { AngularFirestore } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {

  constructor(private afs: AngularFirestore) { }

  listar(){     
    return this.afs.collection<Cliente>('clientes').valueChanges();
  } 

  registrar(cliente: Cliente) {
    return this.afs.collection('clientes').doc(cliente.id).set({
      id : cliente.id,
      nombreCompleto : cliente.nombreCompleto,
      edad : cliente.edad,
      dni : cliente.dni,
    })
  }
 
  registrarConsumo(cliente: Cliente) {
    return this.afs.collection('clientes').add(JSON.parse(JSON.stringify(cliente)));
  }
  
  leer(documentId: string) {
    return this.afs.collection<Cliente>('clientes').doc(documentId).valueChanges();
  }

  modificar(cliente : Cliente){
    return this.afs.collection('clientes').doc(cliente.id).set(JSON.parse(JSON.stringify(cliente)));
  }

  eliminar(cliente: Cliente) {
    return this.afs.collection('clientes').doc(cliente.id).delete();
  }
}

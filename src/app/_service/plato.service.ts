import { Plato } from './../_model/plato';
import { AngularFirestore } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { PlatoComponent } from '../pages/plato/plato.component';

@Injectable({
  providedIn: 'root'
})
export class PlatoService {

  constructor(private afs : AngularFirestore) { 
    
  }

  listar(){    
    return this.afs.collection<Plato>('platos').valueChanges();
    //return this.afs.collection('platos').snapshotChanges();
  }

  registrar(plato: Plato){
    //let idGenerado = this.afs.createId();
    return this.afs.collection('platos').doc(plato.id).set({
      id : plato.id,
      nombre : plato.nombre,
      precio : plato.precio
    })
    //return this.afs.collection('platos').add(JSON.parse(JSON.stringify(plato)));
  }

  leer(documentId: string) {
    return this.afs.collection<Plato>('platos').doc(documentId).valueChanges();
  }

  modificar(plato : Plato){
    return this.afs.collection('platos').doc(plato.id).set(JSON.parse(JSON.stringify(plato)));
  }

  eliminar(plato: Plato) {
    return this.afs.collection('platos').doc(plato.id).delete();
  }
}

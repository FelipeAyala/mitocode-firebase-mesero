import { Menu } from './../_model/menu';
import { Subject } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  menu = new Subject<Menu[]>();

  constructor(private afs: AngularFirestore) { }

  listar(){
    return this.afs.collection<Menu>('menus').valueChanges();
  }
}

import { ReporteComponent } from './pages/reporte/reporte.component';
import { PerfilComponent } from './pages/perfil/perfil.component';
import { Not403Component } from './pages/not403/not403.component';
import { LoginComponent } from './login/login.component';
import { ClienteComponent } from './pages/cliente/cliente.component';
import { ConsultaComponent } from './pages/consulta/consulta.component';
import { ConsumoComponent } from './pages/consumo/consumo.component';
import { Not404Component } from './pages/not404/not404.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlatoComponent } from './pages/plato/plato.component';
import { PlatoEdicionComponent } from './pages/plato/plato-edicion/plato-edicion.component';
import { LoginGuardService } from './_service/login-guard.service';
import { ClienteEdicionComponent } from './pages/cliente/cliente-edicion/cliente-edicion.component';

const routes: Routes = [
  {
    path: 'plato', component: PlatoComponent, children: [
      { path: 'nuevo', component: PlatoEdicionComponent },
      { path: 'edicion/:id', component: PlatoEdicionComponent }
    ], canActivate: [LoginGuardService]
  },
  { path: 'consumo', component: ConsumoComponent, canActivate: [LoginGuardService] },
  { path: 'consulta', component: ConsultaComponent, canActivate: [LoginGuardService] },
  {
    path: 'cliente', component: ClienteComponent, children: [
      { path: 'nuevo', component: ClienteEdicionComponent },
      { path: 'edicion/:id', component: ClienteEdicionComponent }
    ], canActivate: [LoginGuardService]
  },
  { path: 'perfil', component: PerfilComponent, canActivate: [LoginGuardService] },
  { path: 'reporte', component: ReporteComponent, canActivate: [LoginGuardService] },
  { path: 'not-403', component: Not403Component },  
  { path: 'login', component: LoginComponent },
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: '**', component: Not404Component }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

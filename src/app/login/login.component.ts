import { Menu } from './../_model/menu';
import { MenuService } from './../_service/menu.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { LoginService } from './../_service/login.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { auth } from 'firebase/app';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {

  private ngUnsubscribe: Subject<void> = new Subject();

  usuario: string;
  clave: string;
  mensaje: string;
  mensajeCrear: string;
  mensajeRestablecer: string;

  estadoLogin: boolean = true;
  estadoRecuperar: boolean;
  estadoCrear: boolean;

  constructor(private loginService: LoginService, private router: Router, private menuService: MenuService, public afa: AngularFireAuth) { }

  ngOnInit() {
    return this.afa.authState.subscribe(data => {
      if (data != null) {
        this.router.navigate(['/plato']);
      }
    });
  }

  login() {
    this.loginService.login(this.usuario, this.clave).then(() => {
      this.listarMenus();
    });
  }

  loginGoogle() {
    this.loginService.loginGoogle().then(() => {
      this.listarMenus();
    });
  }

  loginFacebook() {
    this.loginService.loginFacebook()
      .then(() => {
        this.listarMenus();
      })
      .catch(err => {
        if (err.code === 'auth/account-exists-with-different-credential') {
          let facebookCred = err.credential;
          let googleProvider = new auth.GoogleAuthProvider();
          googleProvider.setCustomParameters({ 'login_hint': err.email });

          return auth().signInWithPopup(googleProvider).then(result => {
            result.user.linkWithCredential(facebookCred);
            this.router.navigate(['/plato']);
          })
        }
      });
  }

  restablecerClave() {
    this.loginService.restablecerClave(this.usuario);
  }

  crear() {
    this.estadoCrear = true;
    this.estadoLogin = false;
  }

  recuperar() {
    this.estadoRecuperar = true;
    this.estadoLogin = false;
  }

  crearUsuario() {
    this.loginService.registrarUsuario(this.usuario, this.clave).then(res => console.log(res));
  }

  irLogin() {
    this.estadoLogin = true;
    this.estadoRecuperar = false;
    this.estadoCrear = false;
  }

  listarMenus() {

    this.menuService.listar().pipe(takeUntil(this.ngUnsubscribe)).subscribe((data) => {
      let menus: Menu[] = data;   

      this.loginService.user.pipe(takeUntil(this.ngUnsubscribe)).subscribe((data) => {

        //let user_roles: string[] = ['USER', 'DBA', 'SYS', 'ADMIN'];
        if (data) {
          let user_roles: string[] = JSON.parse(JSON.stringify(data)).roles;

          let final_menus: Menu[] = [];

          for (let menu of menus) {
            n2: for (let rol of menu.roles) {
              for (let urol of user_roles) {
                if (rol === urol) {
                  let m = new Menu();
                  m.nombre = menu.nombre;
                  m.icono = menu.icono;
                  m.url = menu.url;
                  final_menus.push(m);
                  break n2;
                }
              }
            }

            this.menuService.menu.next(final_menus);
            //console.log(menus);
            //necesario la navegacion al final para que termine el proceso de menus
            this.router.navigate(['/plato']);
          }
        }
      });
    });
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }



}

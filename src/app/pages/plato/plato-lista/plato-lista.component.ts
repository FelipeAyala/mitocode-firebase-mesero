import { PlatoService } from 'src/app/_service/plato.service';
import { Plato } from './../../../_model/plato';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatSnackBar } from '@angular/material';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-plato-lista',
  templateUrl: './plato-lista.component.html',
  styleUrls: ['./plato-lista.component.css']
})
export class PlatoListaComponent implements OnInit, OnDestroy {

  private ngUnsubscribe: Subject<void> = new Subject();

  platos: Plato[] = [];
  dataSource: MatTableDataSource<Plato>
  displayedColumns = ['nombre', 'precio', 'acciones'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private platoService: PlatoService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.platoService.listar().pipe(takeUntil(this.ngUnsubscribe)).subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  eliminar(p: Plato) {
    this.platoService.eliminar(p);

    this.snackBar.open('SE ELIMINO', 'AVISO', {
      duration: 2000,
    });
  }

  ngOnDestroy(){
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}

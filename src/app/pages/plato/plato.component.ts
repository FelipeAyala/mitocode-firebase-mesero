import { Plato } from './../../_model/plato';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { PlatoService } from 'src/app/_service/plato.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-plato',
  templateUrl: './plato.component.html',
  styleUrls: ['./plato.component.css']
})
export class PlatoComponent implements OnInit, OnDestroy {

  private ngUnsubscribe: Subject<void> = new Subject();
  nombre : string;
  precio: number;

  lista : Plato[] = [];
  constructor(private platoService : PlatoService) { 
    
  }

  ngOnInit() {
    this.platoService.listar().pipe(takeUntil(this.ngUnsubscribe)).subscribe(data => {
      
      /*this.lista = [];
      data.forEach((x : any) => {
        let plato = new Plato();
        plato.id = x.payload.doc.id;
        plato.nombre = x.payload.doc.data().nombre;
        plato.precio = x.payload.doc.data().precio;
        this.lista.push(plato);
      });*/
      
      this.lista = data;
    });
  }

  registrar(){
    let p = new Plato();
    p.nombre = this.nombre;
    p.precio = this.precio;

    this.platoService.registrar(p);
  }

  modificar(p : Plato){
    this.platoService.modificar(p);
  }

  eliminar(p: Plato){
    this.platoService.eliminar(p);
  }

  ngOnDestroy(){
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}

import { Consumo } from 'src/app/_model/consumo';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AngularFireStorage } from '@angular/fire/storage';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-dialogo',
  templateUrl: './dialogo.component.html',
  styleUrls: ['./dialogo.component.css']
})
export class DialogoComponent implements OnInit {

  consumo: Consumo;
  urlImagen: string;
  private ngUnsubscribe: Subject<void> = new Subject();

  constructor(private dialogRef: MatDialogRef<DialogoComponent>, 
    @Inject(MAT_DIALOG_DATA) private data: Consumo,
    private afStorage: AngularFireStorage) { }

  ngOnInit() {
    this.consumo = this.data;
    console.log(this.data.cliente.id);
    if (this.data.cliente.id != null) {
      this.afStorage.ref(`clientes/${this.data.cliente.id}`).getDownloadURL().pipe(takeUntil(this.ngUnsubscribe)).subscribe(data => {
        console.log(data);
        this.urlImagen = data;
      });
    }

  }

  cerrar(){
    this.dialogRef.close();
  }

}

import { AngularFirestore } from '@angular/fire/firestore';
import { MatSnackBar } from '@angular/material';
import { Cliente } from './../../../_model/cliente';
import { ClienteService } from 'src/app/_service/cliente.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { AngularFireStorage, AngularFireStorageReference, AngularFireUploadTask } from '@angular/fire/storage';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-cliente-edicion',
  templateUrl: './cliente-edicion.component.html',
  styleUrls: ['./cliente-edicion.component.css']
})
export class ClienteEdicionComponent implements OnInit, OnDestroy {

  private ngUnsubscribe: Subject<void> = new Subject();

  form: FormGroup;
  id: string;
  edicion: boolean;

  labelFile: string;
  file: any;
  ref: AngularFireStorageReference;
  task: AngularFireUploadTask;
  urlImagen: string;

  constructor(private route: ActivatedRoute, private router: Router, private clienteService: ClienteService, private snackBar: MatSnackBar,
    private afStorage: AngularFireStorage, private afs: AngularFirestore) { }

  ngOnInit() {
    this.form = new FormGroup({
      'id': new FormControl(''),
      'nombre': new FormControl(''),
      'dni': new FormControl(''),
      'edad': new FormControl(0)
    });

    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.edicion = this.id != null;
      this.initForm();
    });
  }

  initForm() {
    if (this.edicion) {
      this.clienteService.leer(this.id).pipe(takeUntil(this.ngUnsubscribe)).subscribe((data: Cliente) => {
        
        this.form = new FormGroup({
          'id': new FormControl(data.id),
          'nombre': new FormControl(data.nombreCompleto),
          'edad': new FormControl(data.edad),
          'dni': new FormControl(data.dni)
        });

        if (data.id != null) {
          this.afStorage.ref(`clientes/${data.id}`).getDownloadURL().pipe(takeUntil(this.ngUnsubscribe)).subscribe(data => {
            this.urlImagen = data;
          });
        }
      });
    }
  }

  operar() {
    let nuevoCliente = new Cliente();
    nuevoCliente.id = this.id;
    nuevoCliente.nombreCompleto = this.form.value['nombre'];
    nuevoCliente.dni = this.form.value['dni'];
    nuevoCliente.edad = this.form.value['edad'];
    let mensaje = '';

    if (this.edicion) {
      this.clienteService.modificar(nuevoCliente);
      mensaje = 'SE MODIFICO';
    } else {
      nuevoCliente.id = this.afs.createId();
      this.clienteService.registrar(nuevoCliente);
      mensaje = 'SE REGISTRO';
    }

    //se guarda la imagen en storage
    if (this.file != null) {
      this.ref = this.afStorage.ref(`clientes/${nuevoCliente.id}`);
      this.task = this.ref.put(this.file);
      this.afStorage.ref
    }

    this.snackBar.open(mensaje, 'AVISO', {
      duration: 2000,
    });

    this.router.navigate(['cliente']);
  }

  seleccionar(e: any) {
    //console.log(e);
    this.file = e.target.files[0];
    this.labelFile = e.target.files[0].name;
  }

  ngOnDestroy(){
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}

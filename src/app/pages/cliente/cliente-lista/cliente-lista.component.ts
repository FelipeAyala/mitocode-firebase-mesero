import { ClienteService } from 'src/app/_service/cliente.service';
import { Cliente } from '../../../_model/cliente';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatSnackBar } from '@angular/material';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-cliente-lista',
  templateUrl: './cliente-lista.component.html',
  styleUrls: ['./cliente-lista.component.css']
})
export class ClienteListaComponent implements OnInit, OnDestroy {

  private ngUnsubscribe: Subject<void> = new Subject();

  clientes: Cliente[] = [];
  dataSource: MatTableDataSource<Cliente>
  displayedColumns = ['nombre', 'dni', 'edad', 'acciones'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private clienteService: ClienteService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.clienteService.listar().pipe(takeUntil(this.ngUnsubscribe)).subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  eliminar(p: Cliente) {
    this.clienteService.eliminar(p);

    this.snackBar.open('SE ELIMINO', 'AVISO', {
      duration: 2000,
    });
  }

  ngOnDestroy(){
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Cliente } from './../../_model/cliente';
import { ClienteService } from './../../_service/cliente.service';
@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.css']
})
export class ClienteComponent implements OnInit, OnDestroy {

  private ngUnsubscribe: Subject<void> = new Subject();
  id : number;
  dni: string;
  nombreCompleto : string;
  edad: number;

  lista : Cliente[] = [];
  constructor(private clienteService : ClienteService) { 
    
  }

  ngOnInit() {
    this.clienteService.listar().pipe(takeUntil(this.ngUnsubscribe)).subscribe(data => { 
      this.lista = data;
    });
  }

  registrar(){
    let p = new Cliente();
    p.dni = this.dni;
    p.edad = this.edad;
    p.nombreCompleto = this.nombreCompleto;

    this.clienteService.registrar(p);
  }

  modificar(p : Cliente){
    this.clienteService.modificar(p);
  }

  eliminar(p: Cliente){
    this.clienteService.eliminar(p);
  }

  ngOnDestroy(){
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}

import { Subject } from 'rxjs';
import { LoginService } from './../../_service/login.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-not403',
  templateUrl: './not403.component.html',
  styleUrls: ['./not403.component.css']
})
export class Not403Component implements OnInit, OnDestroy {

  private ngUnsubscribe: Subject<void> = new Subject();

  usuario: string;

  constructor(private loginService: LoginService) { }

  ngOnInit() {
    this.loginService.user.pipe(takeUntil(this.ngUnsubscribe)).subscribe(data => {
      this.usuario = data.email;
    });  
  }

  ngOnDestroy(){
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}
